var net = require('net');
var EventEmitter = require('events').EventEmitter;

var common = require('./common'), escapeEmail = common.escapeEmail, copy = common.copy;

function Client(host, port, hostname) {
	var thisClient = this;
	this.host = host;
	this.port = port || 587;
	this.greeting = null;
	var extensions = null;
	this.extensions = function (callback) {
		if (extensions) {
			setTimeout(callback.bind(null, copy(extensions)), 0);
		} else {
			thisClient.on('connect', function () {
				thisClient.extensions(callback);
			});
		}
	};
	
	var timeoutMs = 20000, timeoutTimer;
	function updateTimeout(clearOnly) {
		clearTimeout(timeoutTimer);
		if (!clearOnly) {
			timeoutTimer = setTimeout(function () {
				thisClient.emit('error', new Error('Timeout'));
				socket.close();
			}, timeoutMs);
		}
	}
	
	var happyToEnd = false, happyToSendCommands = false;

	var socket = net.connect(port, host);
	
	var commandQueue = [];
	var pendingCallback = function initialResponseFromServer(error, code, text) {
		updateTimeout(true);
		if (error) return thisClient.emit('error', error);
		if (code != 220) return thisClient.emit('error', new Error('Invalid initial response:' + code + ' ' + text));

		// EHLO/HELO first
		thisClient._immediateCommand('EHLO ' + hostname, function (error, code, text) {
			if (error) {
				return thisClient.immediateCommand('HELO ' + hostname, function (error, response) {
					if (error) {
						return thisClient.emit('error', error);
					}
					thisClient.emit('connect', extensions = {});
				});
			}
			extensions = {};
			var lines = text.split('\r\n');
			thisClient.greeting = lines.shift();
			lines.forEach(function (line) {
				var name = line.split(/\s/, 1)[0];
				var args = line.substring(name.length).replace(/^\s+/, '');
				extensions[name.toUpperCase()] = args;
			});
			thisClient.emit('connect', extensions);
		});
		// Allow commands to run
		happyToSendCommands = true;
	};
	function nextCommand() {
		if (!commandQueue.length) {
			pendingCallback = null;
			return;
		}
		var cmd = commandQueue.shift();
		pendingCallback = cmd.callback;
		happyToEnd = cmd.happyToEnd;

		if (/\r\n/.test(cmd.cmd)) return thisClient.emit('error', new Error('Invalid command: ' + cmd.cmd));
		var line = cmd.cmd + '\r\n';
		thisClient.emit('debug', line);
		socket.write(line);
		updateTimeout();
	}
	var multilineCode = null, multilineText = '';
	function processLine(line) {
		if (!pendingCallback) {
			return thisClient.emit('error', new Error('Unexpected response from server'));
		}
		var code = line.split(/(\s|-)/, 1)[0];
		var separator = line[code.length];
		var text = line.substring(code.length).replace(/^(\s+|-)/, '');
		if (multilineCode !== null) {
			if (multilineCode !== code) {
				var error = new Error('Mismatch for multi-line response (expected ' + multilineCode + ', got' + code + ')');
				thisClient.emitError('error', error);
				updateTimeout(true);
				return pendingCallback(error);
			}
			text = multilineText += text + '\r\n';
			if (separator === '-') return;
		} else if (separator === '-') {
			multilineCode = code;
			multilineText = text + '\r\n';
			return;
		}
		var codeNum = parseInt(code, 10);
		if (isNaN(codeNum)) {
			var error = new Error('invalid response code: ' + code);
			thisClient.emitError('error', error);
			return pendingCallback(error);
		}
		if (codeNum < 200 || codeNum >= 400) {
			pendingCallback(new Error(line), codeNum, text);
		} else {
			pendingCallback(null, codeNum, text);
		}
		multilineText = '';
		multilineCode = null;
		nextCommand();
	}
	this._immediateCommand = function (cmd, callback) {
		commandQueue.unshift({cmd: cmd, callback: callback});
		if (happyToSendCommands && !pendingCallback) {
			nextCommand();
		}
	};
	this._command = function (cmd, callback, happyToEnd) {
		commandQueue.push({cmd: cmd, callback: callback, happyToEnd: happyToEnd});
		if (happyToSendCommands && !pendingCallback) {
			nextCommand();
		}
	};
	this._sendData = function (buffer, callback) {
		this._command('DATA', function (error) {
			if (error) return callback(error);

			var text = buffer.toString('ascii'); // Text-only for now
			var lines = text.split('\r\n');
			// Remove trailing newline
			if (lines[lines.length - 1] === '') lines.pop();
			lines.forEach(function (line) {
				// TODO split lines if needed;
				if (line[0] === '.') {
					line = '.' + line;
				}
				thisClient.emit('debug', line);
				socket.write(line + '\r\n');
			});
			thisClient._immediateCommand('.', callback);
		});
	};
	this._closeSocket = function (clean) {
		happyToEnd = happyToEnd || clean;
		socket.destroy();
	};
	
	var pendingReplyText = '';
	socket.on('data', function (buffer) {
		updateTimeout();
		var lines = buffer.toString('ascii').split('\r\n');
		lines[0] = pendingReplyText + lines[0];
		while (lines.length > 1) {
			processLine(lines.shift());
		}
		pendingReplyText = lines[0];
	});
	socket.on('error', function (error) {
		happyToEnd = true;
		thisClient.emit('error', error);
		updateTimeout(true);
	});
	socket.on('close', function () {
		if (!happyToEnd) {
			happyToEnd = true;
			thisClient.emit('error', new Error('Remote server hung up unexpectedly'));
		}
		thisClient.emit('close');
		updateTimeout(true);
	});
	
	socket.on('connect', function () {
		// We're expecting some data (the greeting) immediately, so a timeout is appropriate
		updateTimeout();
	});
}
Client.prototype = Object.create(EventEmitter.prototype);
Client.prototype._commandChain = function (cmds, callback) {
	var thisClient = this;
	cmds = cmds.slice(0);
	function next(error) {
		if (error || !cmds.length) {
			return callback(error);
		}
		var cmd = cmds.shift();
		thisClient._command(cmd, next);
	}
	next();
};
Client.prototype.close = function (callback) {
	var thisClient = this;
	if (callback) this.once('close', callback);
	
	this._command('QUIT', function (error) {
		if (error) return thisClient.emit('error', error);
		thisClient._closeSocket(true);
	}, true);
};
Client.prototype.sendRaw = function (envelope, callback) {
	var thisClient = this;
	var from = envelope.from || '';
	var to = [].concat(envelope.to);
	var commands = ['MAIL FROM:' + escapeEmail(from)].concat(to.map(function (to) {
		return 'RCPT TO:' + escapeEmail(to);
	}));
	this._commandChain(commands, function (error) {
		if (error) return callback(error);
		thisClient._sendData(envelope.data, function (error, code, message) {
			if (error) return callback(error);
			callback(null, code, message)
		});
	});
};

module.exports = Client;