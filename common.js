module.exports = {
	escapeEmail: function escapeEmail(email) {
		return '<' + email.replace(/</g, '%3C').replace(/>/g, '%3E') + '>';
	},
	unescapeEmail: function unescapeEmail(email) {
		return email.replace(/^.*</, '').replace(/>.*/, '').replace(/%3C/g, '<').replace(/%3E/g, '>');
	},
	copy: function copy(obj) {
		return JSON.parse(JSON.stringify(obj));
	}
};