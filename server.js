var stream = require('stream'), Readable = stream.Readable;
var EventEmitter = require('events').EventEmitter;

var common = require('./common');

function Server(ports, netModule, serverOptions) {
	var thisServer = this;
	netModule = netModule || require('net');
	serverOptions = serverOptions || {};
	if (typeof ports === 'number') ports = [ports]; // Support for port 0
	ports = ports || [587, 25];
	var ports = [].concat(ports);
	
	var pending = ports.length;
	var servers = this._servers = [];
	ports.forEach(function (port) {
		var options = port;
		var server = netModule.createServer(serverOptions, onConnection);
		servers.push(server);
		server.listen(options, function (error) {
			if (error) {
				pending = 0;
				thisServer.emit('error', error);
				thisServer.close();
				return;
			}
			
			if (!--pending) {
				thisServer.emit('listening');
			}
		});
		server.on('error', function (error) {
			thisServer.emit('error', error);
		});
		server.on('close', function () {
			var index = servers.indexOf(server);
			servers.splice(index, 1);
			if (!servers.length) thisServer.emit('close');
		});
	});
	
	function onConnection(socket) {
		var connection = new Connection(thisServer, socket);
		connection.on('error', function (error) {
			thisServer.emit('error', error);
			connection.close();
		});
		connection.on('message', function (message) {
			thisServer.emit('message', message);
		})
	}
}
Server.prototype = Object.create(EventEmitter.prototype);
Server.prototype.close = function (callback) {
	if (callback) {
		this.once('close', callback);
	}
	this._servers.forEach(function (server) {
		server.close();
	});
};
Server.prototype.addresses = function () {
	return this._servers.map(function (server) {
		return server.address();
	});
};

function Connection(server, socket) {
	var thisConnection = this;
	this._server = server;
	this._socket = socket;
	var address = socket.address();
	var host = address.address + ':' + address.port;

	// Initial connection message
	this.sendText(220, host, 'diddly-woot');
	
	var happyToEnd = false;
	var messageStream = null;

	var pendingReplyText = '';
	socket.on('data', function (buffer) {
		server.emit('debug', 'received: ' + buffer);
		if (messageStream) {
			var buffer = messageStream._inputBuffer(buffer);
			if (!buffer && !messageStream.finished()) {
				return;
			} else {
				thisConnection.sendText('250 OK');
				messageStream = null;
				socket.resume();
			}
		}
		
		// TODO: what if this is not text
		var lines = buffer.toString('ascii').split('\r\n');
		lines[0] = pendingReplyText + lines[0];
		while (lines.length > 1) {
			processLine(lines.shift());
		}
		pendingReplyText = lines[0];
	});
	socket.on('close', function () {
		if (!happyToEnd) {
			return thisConnection.emit('error', new Error('Client hung up unexpectedly'));
		}
		thisConnection.emit('close');
	});
	
	var pendingEmail = null;
	function resetEmail() {
		pendingEmail = {
			to: [],
			from: null,
		};
	}
	
	function processLine(line) {
		var command = line.split(/\s/, 1)[0];
		var text = line.substring(command.length).replace(/^\s+/, '');
		command = command.toUpperCase(); // Have to do this down here, in case of capitals nonsense
		
		if (command === 'EHLO') {
			resetEmail();
			thisConnection.sendText('250-It\'s a good day to be ' + text + '\r\n250 PIPELINING');
		} else if (command === 'HELO') {
			resetEmail();
			thisConnection.sendText('250 It\'s a good day to be ' + text);
		} else if (command === 'MAIL') {
			if (!pendingEmail) {
				return thisConnection.sendText('503 EHLO/HELO not sent');
			}
			command = text.split(':', 1)[0];
			text = text.substring(command.length + 1).replace(/^\s+/, '');
			command = command.toUpperCase();
			if (command === 'FROM') {
				resetEmail();
				pendingEmail.from = text;
				thisConnection.sendText('250 OK');
			} else {
				thisConnection.emit('error', new Error('Unknown MAIL command: ' + line));
			}
		} else if (command === 'RCPT') {
			if (!pendingEmail) {
				return thisConnection.sendText('503 EHLO/HELO not sent');
			}
			command = text.split(':', 1)[0];
			text = text.substring(command.length + 1).replace(/^\s+/, '');
			command = command.toUpperCase();
			if (command === 'TO') {
				pendingEmail.to.push(text);
				thisConnection.sendText('252 OK');
			} else {
				thisConnection.emit('error', new Error('Unknown RCPT command: ' + line));
			}
		} else if (command === 'DATA') {
			if (!pendingEmail) {
				return thisConnection.sendText('503 EHLO/HELO not sent');
			}
			if (!pendingEmail.to.length) {
				return thisConnection.sendText('554 No valid recipients');
			}
			messageStream = new MessageStream(socket, pendingEmail);
			thisConnection.sendText('354 End data with <CRLF>.<CRLF>');
			thisConnection.emit('message', messageStream);
			resetEmail();
		} else if (command === 'RSET') {
			if (!pendingEmail) {
				return thisConnection.sendText('503 EHLO/HELO not sent');
			}
			resetEmail();
			thisConnection.sendText('250 OK');
		} else if (command === 'QUIT') {
			happyToEnd = true;
			thisConnection.sendText('221 PEACE OUT <3');
			thisConnection.close();
		} else if (command === 'VRFY') {
			throw new Error('VRFY not implemented');
		} else if (command === 'EXPN') {
			thisConnection.sendText('502 EXPN not supported');
		} else if (command === 'NOOP') {
			thisConnection.sendText('250 OK');
		} else if (command === 'HELP') {
			thisConnection.sendText('214 Sorry, but you\'re on your own');
		} else {
			thisConnection.sendText('500 unknown command: ' + command);
		}
		// See: <http://tools.ietf.org/html/rfc5321#section-4.2.2> for response codes
	}
}
Connection.prototype = Object.create(EventEmitter.prototype);
Connection.prototype.close = function () {
	this._socket.destroy();
};
Connection.prototype.sendText = function () {
	var args = [];
	for (var i = 0; i < arguments.length; i++) {
		args.push(arguments[i]);
	}
	var line = args.join(' ') + '\r\n';
	this._server.emit('debug', line);
	var socket = this._socket;
	this._socket.write(line);
};

function MessageStream(socket, envelope, options) {
	var thisStream = this;
	Readable.call(this, options);
	
	this.to = envelope.to;
	this.from = common.unescapeEmail(envelope.from);
	
	var finished = false;
	this.finished = function () {
		return finished;
	};
	
	socket.pause();
	this._read = function (bytes) {
		socket.resume();
		return null;
	};

	this._inputBuffer = function (buffer) {
		// TODO: text only for now
		var lines = buffer.toString('ascii').split('\r\n');
		var done = false;
		while (lines.length > 1) {
			var line = lines.shift();
			if (line === '.') {
				done = true;
				break;
			} else {
				if (line[0] === '.') line = line.substring(1);
				if (!thisStream.push(line)) {
					// They don't want any more
					socket.pause();
				}
			}
		}
		if (done) {
			finished = true;
			thisStream.push(null);
			return new Buffer(lines.join('\r\n'), 'ascii');
		} else {
			return null;
		}
	};
}
MessageStream.prototype = Object.create(Readable.prototype);
MessageStream.prototype.getData = function (callback) {
	var buffers = [];
	this.on('data', function (data) {
		buffers.push(data);
	});
	this.on('error', function (error) {
		buffers = null;
		return callback(error);
	});
	this.on('end', function () {
		if (buffers) {
			callback(null, Buffer.concat(buffers));
		}
	})
};

module.exports = Server;