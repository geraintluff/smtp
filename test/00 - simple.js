var assert = require('chai').assert;

var smtp = require('../');

describe('simple use', function () {
	var server, client;
	
	beforeEach(function (done) {
		server = new smtp.Server(0);
		var count = 0;
		server.on('listening', function () {
			var address = server.addresses()[0];
			client = new smtp.Client('localhost', address.port);
			client.on('error', function (error) {
				throw error;
			})
			client.on('debug', function (line) {
				console.log('client:', line.replace(/[\s\r\n]*$/, ''));
			});

			done();
		});
		server.on('error', function (error) {
			throw error;
		})
		server.on('debug', function (line) {
			console.log('server:', line.replace(/[\s\r\n]*$/, ''));
		});
	});
	
	afterEach(function (done) {
		client.close();
		server.close(function (error) {
			if (error) throw error;
			done();
		});
	});
	
	it('sends', function (done) {
		var address = server.addresses()[0];
		var email = 'test@localhost:' + address.port;

		client.extensions(function (extensions) {
			assert.isObject(extensions);
			
			server.on('message', function (message) {
				assert.equal(message.from, 'jane@example.com');
				message.resume(); // force flowing mode
				message.on('end', done);
			});
			
			client.sendRaw({
				from: 'jane@example.com',
				to: ['test-address@localhost'],
				data: new Buffer('OH HAI\nIt\'s me\r\nfoo bar\r\n.what\r\n', 'utf-8')
			}, function (error) {
				if (error) throw error;
			});
		});
	});
});