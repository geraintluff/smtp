var assert = require('chai').assert;

var smtp = require('../');
var SMTPConnection = require('smtp-connection');

describe('test server against smtp-connection library', function () {
	var server;
	
	beforeEach(function (done) {
		server = new smtp.Server(0);
		var count = 0;
		server.on('listening', function () {
			var address = server.addresses()[0];
			done();
		});
		server.on('error', function (error) {
			throw error;
		})
		server.on('debug', function (line) {
			console.log('server:', line.replace(/[\s\r\n]*$/, ''));
		});
	});
	
	afterEach(function (done) {
		server.close(function (error) {
			if (error) throw error;
			done();
		});
	});
	
	it('sends', function (done) {
		var address = server.addresses()[0];
		var client = new SMTPConnection({
			port: address.port,
			host: 'localhost'
		});
		client.connect(function (error) {
			if (error) throw error;
			
			server.on('message', function (message) {
				message.getData(function (error, data) {
					done();
				});
			});
			
			client.send({
				from: 'jane@example.com',
				to: ['test1@localhost:' + address.port]
			}, 'OH HAI', function (error) {
				if (error) throw error;
				client.quit();
			})
		});
	});
});